<?php

namespace App\Services;

use App\Models\Customer\Customer;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PHPShopify;

class OrderService
{
    /**
     * @param array $customers
     * @return Order
     * @throws \Exception
     */
    public function createByCustomers(array $customers): Order
    {
        if (empty($customers)) {
            throw new Exception('Data is empty.');
        }

        DB::beginTransaction();

        try {
            $order = new Order();
            $order->save();

            $currentCountry = geoip()->getLocation(request()->ip())->country;

            foreach ($customers as $number => $customerData) {
                $customer = new Customer();
                $customer->fill($customerData);
                $customer->country = $currentCountry;
                $customer->order_id = $order->id;
                $customer->lead = (int) ($number === 0);
                $customer->save();
            }

            DB::commit();
        } catch (\Exception $e) {

            DB::rollBack();
            throw $e;
        }

        return $order;
    }

    /**
     * @param string $name
     * @return array
     */
    public function searchCustomerByName(string $name)
    {
        $conf = \config('shopify');
        $shopify = PHPShopify\ShopifySDK::config($conf);

        $location = geoip()->getLocation(request()->ip())->country;
        $search = $name . ' country:' . $location;

        return $shopify->Customer->search($search);
    }

}