<?php

namespace App\Http;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customers.*.name' => 'required|string|max:50',
            'customers.*.phone' => 'required|regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/|max:25',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customers.*.name.required' => 'Name is required!',
            'customers.*.phone.required' => 'Phone is required!',
            'customers.*.name.string' => 'Name is string!',
            'customers.*.phone.regex' => 'Incorrect phone number!'
        ];
    }

}