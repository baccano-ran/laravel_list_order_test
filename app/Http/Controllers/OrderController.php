<?php

namespace App\Http\Controllers;

use App\Http\CustomerRequest;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    public function create()
    {
        return view('order.create');
    }

    public function store(CustomerRequest $requests)
    {
        $customers = (array) $requests->get('customers', []);
        /** @var OrderService $orderService */
        $orderService = Container::getInstance()->make(OrderService::class);

        try {
            $order = $orderService->createByCustomers($customers);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => $e->getMessage()])
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([
            'order' => $order,
        ]);
    }

    public function index()
    {
        $orders = Order::all();

        return view('order.index', [
            'orders' => $orders,
        ]);
    }

    public function search(Request $request)
    {
        if (!$request->isMethod('post')) {
            return response()->json(['response' => 'This is get method']);
        }

        /** @var OrderService $orderService */
        $orderService = Container::getInstance()->make(OrderService::class);
        $customerName = (string) $request->post('name');

        return response()->json([
            'customers' => $orderService->searchCustomerByName($customerName),
        ]);
    }
}