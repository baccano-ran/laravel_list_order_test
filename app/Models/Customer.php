<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App\Models\Customer
 *
 * @property int $id
 * @property int $order_id
 * @property int $lead
 * @property string $name
 * @property string $phone
 * @property string $country
 */
class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = ['name', 'phone'];

    public $timestamps = false;
}
