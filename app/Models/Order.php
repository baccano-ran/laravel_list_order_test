<?php

namespace App\Models;

use App\Models\Customer\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property int $id
 * @property int $updated_at
 * @property int $created_at
 *
 * @property-read Customer[] $customers
 */
class Order extends Model
{
    protected $table = 'orders';

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
