@php( $customerNumber = $customerNumber ?? 0 )

<div class="list-group-item alert" data-customer_number="{{ $customerNumber }}" role="alert">

    <button type="button"
            style="display: none"
            class="js-button-close close"
            data-dismiss="alert"
            aria-label="Close">
        <span aria-hidden="true">
            &times;
        </span>
    </button>

    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('customers[0][name]', '', ['class' => 'form-control js-name-input', 'placeholder' => "Jane Doe" ])}}
        <div class="found-list js-found-list"></div>
        <div class="js-field-errors"></div>
    </div>

    <div class="form-group">
        {{Form::label('phone', 'Phone')}}
        {{Form::text('customers[0][phone]', '', ['class' => 'form-control js-phone-input', 'placeholder' => "Phone"])}}
        <div class="js-field-errors"></div>
    </div>
</div>