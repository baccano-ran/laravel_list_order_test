@extends('layout')

@section('content')
    <template class="js-customer-template">
        @include('order.order-template')
    </template>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                {!! Form::open(['route' => 'order.store', 'id' => 'order-form']) !!}
                <div class="list-group">
                    <h3 class="page-header">Please, fill out the form</h3>
                    <div class="errors"></div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="js-customer-list">
                        @include('order.order-template')
                    </div>
                </div>
                {{ Form::submit('Crete Order', ['class' => 'btn btn-success'])}}
                {{ Form::button('Add Guest', ['class' => 'btn btn-default pull-right js-add-row'])}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection