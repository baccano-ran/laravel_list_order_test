@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>All the Orders</h1>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Phone</td>
                        <td>Country</td>
                        <td>Date create</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        @php( $customers = $order->customers->all() )
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ implode(array_column($customers, 'name'), ', ') }}</td>
                            <td>{{ implode(array_column($customers, 'phone'), ', ') }}</td>
                            <td>{{ implode(array_column($customers, 'country'), ', ') }}</td>
                            <td>{{ $order->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Html::linkRoute('order.create', 'Create order', [], ['class' => 'btn btn-default pull-right']) }}
            </div>
        </div>

    </div>
@endsection