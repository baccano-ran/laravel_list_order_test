const config = {
    ERROR_BLOCK_SELECTOR: '.js-field-errors',
    ERROR_FIELD_CLASS: 'has-error',
    FIELD_CONTAINER_SELECTOR: '.form-group',

    ERROR_MESSAGE_TEMPLATE: '<div class="text-danger">{message}</div>',
};

class FormHelper
{
    /**
     * @param {jQuery} form
     * @param {Object} errors
     */
    static highlightFromErrors(form, errors)
    {
        let fieldName, fieldNameParts, fieldErrors, formGroup, message;

        form.find(config.ERROR_BLOCK_SELECTOR).html('');

        for (fieldName in errors) {
            fieldErrors = errors[fieldName];
            fieldNameParts = fieldName.split('.');
            fieldName = fieldNameParts.slice(0, 1) + fieldNameParts.slice(1).map(function (e) {
                return '[' + e + ']';
            }).join('');

            formGroup = $('[name="' + fieldName + '"]').closest(config.FIELD_CONTAINER_SELECTOR);
            formGroup.addClass(config.ERROR_FIELD_CLASS);

            fieldErrors.forEach(function (error) {
                message = config.ERROR_MESSAGE_TEMPLATE.replace(/{message}/ig, error);
                formGroup.find(config.ERROR_BLOCK_SELECTOR).append(message);
            });
        }
    }
}

export { FormHelper }