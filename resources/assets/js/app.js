import { NewOrderForm } from "./components/order/NewForm";

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function() {
    new NewOrderForm($('#order-form'));
});