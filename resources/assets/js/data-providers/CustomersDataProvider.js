const config = {
    SEARCH_BY_NAME_URL: 'search',
};

class CustomersDataProvider
{
    /**
     * @param {jQuery} input
     * @param {Function} [success]
     */
    static searchByName(input, success)
    {
        if (input.value.length < 2) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: config.SEARCH_BY_NAME_URL,
            data: { 'name': input.value }
        }).done(function (r) {
            if (!r.customers) {
                return;
            }

            if (success instanceof Function) {
                success({
                    customers: r.customers
                });
            }
        });
    };
}

export { CustomersDataProvider }