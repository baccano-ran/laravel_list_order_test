import { FormHelper } from "../../helpers/FormHelper";
import { OrderSearchInput } from "./SearchInput";

const selectors = {
    ADD_CUSTOMER_BUTTON: '.js-add-row',
    NEW_CUSTOMER_TEMPLATE: '.js-customer-template',
    CUSTOMER_LIST: '.js-customer-list',
    NAME_INPUT: '.js-name-input',
    PHONE_INPUT: '.js-phone-input',
    CUSTOMER_CLOSE_BUTTON: '.close',
    FIELD_CONTAINER_SELECTOR: '.form-group'
};

class NewOrderForm
{
    /**
     * @param {jQuery} container
     */
    constructor(container)
    {
        this.container = container;
        this.rowAdded = 0;

        this.initElements();
        this.initEvents();
    }

    initElements()
    {
        let self = this;

        this.addCustomerButton = this.container.find(selectors.ADD_CUSTOMER_BUTTON);
        this.customerList = this.container.find(selectors.CUSTOMER_LIST);

        this.customerTemplate = $(selectors.NEW_CUSTOMER_TEMPLATE).html();

        this.searchInputContainer = this.container.find(selectors.NAME_INPUT).closest(selectors.FIELD_CONTAINER_SELECTOR);

        this.customerSearchInput = new OrderSearchInput(this.searchInputContainer, function(data) {
            NewOrderForm.fillInputsByCustomer(self.searchInputContainer.closest('.list-group-item'), data);
        });
    }

    static fillInputsByCustomer(fieldsContainer, data)
    {
        let customer = data.customer,
            name = customer.first_name + ' ' + customer.last_name,
            phone = customer.phone ? customer.phone : '';

        fieldsContainer.find(selectors.NAME_INPUT).val(name);
        fieldsContainer.find(selectors.PHONE_INPUT).val(phone);
    }

    initEvents()
    {
        let self = this;

        self.addCustomerButton.on('click', function(e) {
            let newItem = $(self.customerTemplate),
                closeBtn = newItem.find(selectors.CUSTOMER_CLOSE_BUTTON);

            self.rowAdded++;

            closeBtn.on('click', function() {
                newItem.remove();
            });

            closeBtn.show();

            newItem.find(selectors.NAME_INPUT).attr('name', 'customers' + '[' + self.rowAdded + ']' + '[name]');
            newItem.find(selectors.PHONE_INPUT).attr('name', 'customers' + '[' + self.rowAdded + ']' + '[phone]');

            new OrderSearchInput(newItem, function(data) {
                NewOrderForm.fillInputsByCustomer(newItem, data);
            });

            self.customerList.append(newItem);
        });

        self.container.on('submit', function (e) {
            let form = $(this),
                formURL = form.attr('action'),
                formMethod = form.attr('method'),
                postData = form.serialize();

            e.preventDefault();

            $.ajax({
                type: formMethod,
                url: formURL,
                data: postData,
            }).done(function(r) {
                location.href = '/';
            }).fail(function(xhr) {
                if (xhr.responseJSON && xhr.responseJSON.errors) {
                    FormHelper.highlightFromErrors(form, xhr.responseJSON.errors);
                }
            });
        });
    }

}

export { NewOrderForm };