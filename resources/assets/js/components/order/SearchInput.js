import { CustomersDataProvider } from '../../data-providers/CustomersDataProvider';

let canSearch = 0;

class OrderSearchInput
{
    /**
     * @param {jQuery} container
     * @param {Function} [onSearchCustomerClick]
     */
    constructor(container, onSearchCustomerClick)
    {
        this.container = container;
        this.onSearchCustomerClick = onSearchCustomerClick;

        this.initElements();
        this.initEvents();
    }

    initElements()
    {
        this.input = this.container.find('.js-name-input');
        this.foundList = this.container.find('.js-found-list');
    }

    initEvents()
    {
        let self = this;

        this.input.on('keyup', function() {
            let input = this;

            canSearch++;

            setTimeout(function () {
                canSearch--;

                if (canSearch !== 0) {
                    return;
                }

                CustomersDataProvider.searchByName(input, self._onFoundCustomers.bind(self));

            }, 500);
        });
    }

    _onFoundCustomers(data)
    {
        let self = this,
            customers = data.customers,
            first_name, last_name, phone, options = '';

        if (!customers.length) {
            return;
        }

        $.each(customers, function (i, customer) {
            first_name = customer.first_name;
            last_name = customer.last_name;
            phone = customer.phone ? customer.phone : '';

            options += "<option data-customer='" + JSON.stringify(customer) + "'>"
                + first_name + ' ' + last_name + ' ' + phone +
                "</option>";
        });

        self.foundList.show(200);
        self.foundList.html(options);

        setTimeout(function () {
            self.foundList.fadeOut(200);
        }, 5000);

        self.foundList.find('option').on('click', function () {
            let option = $(this),
                customer = option.data('customer');

            if (self.onSearchCustomerClick instanceof Function) {
                self.onSearchCustomerClick({ customer: customer });
            }
            self.foundList.fadeOut(200);
        });
    }
}

export { OrderSearchInput }