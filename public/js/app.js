/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(6);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_order_NewForm__ = __webpack_require__(2);


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
    new __WEBPACK_IMPORTED_MODULE_0__components_order_NewForm__["a" /* NewOrderForm */]($('#order-form'));
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewOrderForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helpers_FormHelper__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__SearchInput__ = __webpack_require__(4);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var selectors = {
    ADD_CUSTOMER_BUTTON: '.js-add-row',
    NEW_CUSTOMER_TEMPLATE: '.js-customer-template',
    CUSTOMER_LIST: '.js-customer-list',
    NAME_INPUT: '.js-name-input',
    PHONE_INPUT: '.js-phone-input',
    CUSTOMER_CLOSE_BUTTON: '.close',
    FIELD_CONTAINER_SELECTOR: '.form-group'
};

var NewOrderForm = function () {
    /**
     * @param {jQuery} container
     */
    function NewOrderForm(container) {
        _classCallCheck(this, NewOrderForm);

        this.container = container;
        this.rowAdded = 0;

        this.initElements();
        this.initEvents();
    }

    _createClass(NewOrderForm, [{
        key: "initElements",
        value: function initElements() {
            var self = this;

            this.addCustomerButton = this.container.find(selectors.ADD_CUSTOMER_BUTTON);
            this.customerList = this.container.find(selectors.CUSTOMER_LIST);

            this.customerTemplate = $(selectors.NEW_CUSTOMER_TEMPLATE).html();

            this.searchInputContainer = this.container.find(selectors.NAME_INPUT).closest(selectors.FIELD_CONTAINER_SELECTOR);

            this.customerSearchInput = new __WEBPACK_IMPORTED_MODULE_1__SearchInput__["a" /* OrderSearchInput */](this.searchInputContainer, function (data) {
                NewOrderForm.fillInputsByCustomer(self.searchInputContainer.closest('.list-group-item'), data);
            });
        }
    }, {
        key: "initEvents",
        value: function initEvents() {
            var self = this;

            self.addCustomerButton.on('click', function (e) {
                var newItem = $(self.customerTemplate),
                    closeBtn = newItem.find(selectors.CUSTOMER_CLOSE_BUTTON);

                self.rowAdded++;

                closeBtn.on('click', function () {
                    newItem.remove();
                });

                closeBtn.show();

                newItem.find(selectors.NAME_INPUT).attr('name', 'customers' + '[' + self.rowAdded + ']' + '[name]');
                newItem.find(selectors.PHONE_INPUT).attr('name', 'customers' + '[' + self.rowAdded + ']' + '[phone]');

                new __WEBPACK_IMPORTED_MODULE_1__SearchInput__["a" /* OrderSearchInput */](newItem, function (data) {
                    NewOrderForm.fillInputsByCustomer(newItem, data);
                });

                self.customerList.append(newItem);
            });

            self.container.on('submit', function (e) {
                var form = $(this),
                    formURL = form.attr('action'),
                    formMethod = form.attr('method'),
                    postData = form.serialize();

                e.preventDefault();

                $.ajax({
                    type: formMethod,
                    url: formURL,
                    data: postData
                }).done(function (r) {
                    location.href = '/';
                }).fail(function (xhr) {
                    if (xhr.responseJSON && xhr.responseJSON.errors) {
                        __WEBPACK_IMPORTED_MODULE_0__helpers_FormHelper__["a" /* FormHelper */].highlightFromErrors(form, xhr.responseJSON.errors);
                    }
                });
            });
        }
    }], [{
        key: "fillInputsByCustomer",
        value: function fillInputsByCustomer(fieldsContainer, data) {
            var customer = data.customer,
                name = customer.first_name + ' ' + customer.last_name,
                phone = customer.phone ? customer.phone : '';

            fieldsContainer.find(selectors.NAME_INPUT).val(name);
            fieldsContainer.find(selectors.PHONE_INPUT).val(phone);
        }
    }]);

    return NewOrderForm;
}();



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormHelper; });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var config = {
    ERROR_BLOCK_SELECTOR: '.js-field-errors',
    ERROR_FIELD_CLASS: 'has-error',
    FIELD_CONTAINER_SELECTOR: '.form-group',

    ERROR_MESSAGE_TEMPLATE: '<div class="text-danger">{message}</div>'
};

var FormHelper = function () {
    function FormHelper() {
        _classCallCheck(this, FormHelper);
    }

    _createClass(FormHelper, null, [{
        key: 'highlightFromErrors',

        /**
         * @param {jQuery} form
         * @param {Object} errors
         */
        value: function highlightFromErrors(form, errors) {
            var fieldName = void 0,
                fieldNameParts = void 0,
                fieldErrors = void 0,
                formGroup = void 0,
                message = void 0;

            form.find(config.ERROR_BLOCK_SELECTOR).html('');

            for (fieldName in errors) {
                fieldErrors = errors[fieldName];
                fieldNameParts = fieldName.split('.');
                fieldName = fieldNameParts.slice(0, 1) + fieldNameParts.slice(1).map(function (e) {
                    return '[' + e + ']';
                }).join('');

                formGroup = $('[name="' + fieldName + '"]').closest(config.FIELD_CONTAINER_SELECTOR);
                formGroup.addClass(config.ERROR_FIELD_CLASS);

                fieldErrors.forEach(function (error) {
                    message = config.ERROR_MESSAGE_TEMPLATE.replace(/{message}/ig, error);
                    formGroup.find(config.ERROR_BLOCK_SELECTOR).append(message);
                });
            }
        }
    }]);

    return FormHelper;
}();



/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderSearchInput; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data_providers_CustomersDataProvider__ = __webpack_require__(17);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var canSearch = 0;

var OrderSearchInput = function () {
    /**
     * @param {jQuery} container
     * @param {Function} [onSearchCustomerClick]
     */
    function OrderSearchInput(container, onSearchCustomerClick) {
        _classCallCheck(this, OrderSearchInput);

        this.container = container;
        this.onSearchCustomerClick = onSearchCustomerClick;

        this.initElements();
        this.initEvents();
    }

    _createClass(OrderSearchInput, [{
        key: 'initElements',
        value: function initElements() {
            this.input = this.container.find('.js-name-input');
            this.foundList = this.container.find('.js-found-list');
        }
    }, {
        key: 'initEvents',
        value: function initEvents() {
            var self = this;

            this.input.on('keyup', function () {
                var input = this;

                canSearch++;

                setTimeout(function () {
                    canSearch--;

                    if (canSearch !== 0) {
                        return;
                    }

                    __WEBPACK_IMPORTED_MODULE_0__data_providers_CustomersDataProvider__["a" /* CustomersDataProvider */].searchByName(input, self._onFoundCustomers.bind(self));
                }, 500);
            });
        }
    }, {
        key: '_onFoundCustomers',
        value: function _onFoundCustomers(data) {
            var self = this,
                customers = data.customers,
                first_name = void 0,
                last_name = void 0,
                phone = void 0,
                options = '';

            if (!customers.length) {
                return;
            }

            $.each(customers, function (i, customer) {
                first_name = customer.first_name;
                last_name = customer.last_name;
                phone = customer.phone ? customer.phone : '';

                options += "<option data-customer='" + JSON.stringify(customer) + "'>" + first_name + ' ' + last_name + ' ' + phone + "</option>";
            });

            self.foundList.show(200);
            self.foundList.html(options);

            setTimeout(function () {
                self.foundList.fadeOut(200);
            }, 5000);

            self.foundList.find('option').on('click', function () {
                var option = $(this),
                    customer = option.data('customer');

                if (self.onSearchCustomerClick instanceof Function) {
                    self.onSearchCustomerClick({ customer: customer });
                }
                self.foundList.fadeOut(200);
            });
        }
    }]);

    return OrderSearchInput;
}();



/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersDataProvider; });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var config = {
    SEARCH_BY_NAME_URL: 'search'
};

var CustomersDataProvider = function () {
    function CustomersDataProvider() {
        _classCallCheck(this, CustomersDataProvider);
    }

    _createClass(CustomersDataProvider, null, [{
        key: 'searchByName',

        /**
         * @param {jQuery} input
         * @param {Function} [success]
         */
        value: function searchByName(input, success) {
            if (input.value.length < 2) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: config.SEARCH_BY_NAME_URL,
                data: { 'name': input.value }
            }).done(function (r) {
                if (!r.customers) {
                    return;
                }

                if (success instanceof Function) {
                    success({
                        customers: r.customers
                    });
                }
            });
        }
    }]);

    return CustomersDataProvider;
}();



/***/ })
/******/ ]);