<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('order/create', 'OrderController@create')->name('order.create');
Route::post('order/search', 'OrderController@search')->name('order.search');
Route::post('order/store', 'OrderController@store')->name('order.store');
Route::get('order/index', 'OrderController@index')->name('order.index');
Route::get('order', 'OrderController@index')->name('order.index');

Route::redirect('/', '/order');